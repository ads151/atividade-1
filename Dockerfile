FROM nginx
COPY index.html /usr/share/nginx/html
ARG VAR_ARG=RNP
ENV VAR_ENV=$VAR_ARG
RUN sed -i "s|Hello World|Oi $VAR_ENV|g" /usr/share/nginx/html/index.html
